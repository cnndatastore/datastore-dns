var datastorerpc = require('datastore-rpc');
var datastoredns = require('.');
var io = require('socket.io-client');

var app = {};

app.socketio = io('http://127.0.0.1:8005');
app.rpc = datastorerpc({});
app.rpc.addConnection(app.socketio);
app.server = 'dsnyc1';
app.dns = datastoredns({});
app.dnsClient = app.dns.client(app.rpc, app.server);

app.rpc.on('connect', function() {
	console.log("RPC-NETWORK CONNECTED");
	/* app.dnsClient.groupSubscribe('telestrator-QX16', function(error, addresses) {
		console.log("TELESTRATOR-ADDRESSES", error, addresses);
	}); */

	app.dnsClient.groupSubscribe('telestrator-QX16', function(error, groupDoc) {
		console.log("G-DOC", error, groupDoc);

		app.dnsClient.rpcRequest('telestrator-QX16', '/test', function(error, addresses) {
			console.log("TELESTRATOR-ADDRESSES", error, addresses);
		});
	});


});

