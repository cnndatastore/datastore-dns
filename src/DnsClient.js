var Utils = require('./Utils');

function DnsClient(dnsDb, rpc, dnsServerAddress) {
	this._dnsDb = dnsDb;
	this._rpc = rpc;
	this._dnsServerAddress = dnsServerAddress;
	this._config = {
		apiPrefix: '/api/dns'
	}
}

DnsClient.prototype.rpcRequest = function() {
	var self = this;

	var parsedArgs = Utils.parseArgs(
		arguments,
		[
			{name: 'destGroup', level: 0,  validate: function(arg, allArgs) { return typeof arg == 'string'; }},
			{name: 'path', level: 0,  validate: function(arg, allArgs) { return typeof arg == 'string' && arg[0] == '/'; }},
			{name: 'query', level: 1,  validate: function(arg, allArgs) { return typeof arg == 'object'; }, default: {}},
			{name: 'options', level: 2,  validate: function(arg, allArgs) { return typeof arg == 'object'; }, default: {}},
			{name: 'callback', level: 1,  validate: function(arg, allArgs) { return typeof(arg) === 'function'; }}
		]
	);

	var defaultOptions = {
		groupLookup: true,
		groupSubscribe: false
	};

	parsedArgs.options = Utils.objectMerge(defaultOptions, parsedArgs.options);

	self._dnsDb.groups.findOne(parsedArgs.destGroup, function(error, groupDoc) {
		if(error)
			callbackSafe(error, null);
		else if(groupDoc != null) { // Group is saved locally
			if(Array.isArray(groupDoc.addresses) && groupDoc.addresses.length > 0) // group has members
				self._rpc.request(groupDoc.addresses, parsedArgs.path, parsedArgs.query, parsedArgs.options, parsedArgs.callback);
		}
		else if(parsedArgs.options.groupLookup) { // Try retreiving group remotely
			if(parsedArgs.options.groupSubscribe) {
				self.groupSubscribe(parsedArgs.destGroup, function(error, subscriptionId) {
					self._dnsDb.groups.findOne(parsedArgs.destGroup, function(error, groupDoc) {
						if(Array.isArray(groupDoc.addresses) && groupDoc.addresses.length > 0) // group has members
							self._rpc.request(groupDoc.addresses, parsedArgs.path, parsedArgs.query, parsedArgs.options, parsedArgs.callback);
					});
				});
			}
			else {
				self.groupAddresses(parsedArgs.destGroup, function(error, addresses) {
					if(error)
						callbackSafe(error, null);
					else
						self._rpc.request(addresses, parsedArgs.path, parsedArgs.query, parsedArgs.options, parsedArgs.callback);
				});
			}
		}
		else
			callbackSafe('group_undefined', null);
	});

	function callbackSafe(error, result) {
		if(typeof parsedArgs.callback == 'function')
			parsedArgs.callback(error, result);
	}
};

DnsClient.prototype.joinGroup = function(group, callback) {
	var self = this;
	self._rpc.request(self._dnsServerAddress, self._config.apiPrefix + '/join', {address: self._rpc.address, group: group}, {multipleResponses: false}, callback);
};

DnsClient.prototype.leaveGroup = function(group, callback) {
	var self = this;
	self._rpc.request(self._dnsServerAddress, self._config.apiPrefix + '/leave', {address: self._rpc.address, group: group}, {multipleResponses: false}, callback);
};

DnsClient.prototype.leaveAllGroups = function(callback) {
	var self = this;
	self._rpc.request(self._dnsServerAddress, self._config.apiPrefix + '/leaveall', {address: self._rpc.address}, {multipleResponses: false}, callback);
};

DnsClient.prototype.groupAddresses = function(group, callback) {
	var self = this;
	self._rpc.request(self._dnsServerAddress, self._config.apiPrefix + '/group/find', {group: group}, {multipleResponses: false}, callback);
};

DnsClient.prototype.groupSubscribe = function(group, callback) {
	var self = this;
	self._rpc.request(self._dnsServerAddress, self._config.apiPrefix + '/group/subscribe', {group: group}, {multipleResponses: true}, function(error, operationDoc) {
		if(error)
			callback(error, null);
		else if(operationDoc.operation == 'subscribe')
			callback(null, operationDoc.subscriptionId);
		else
			self._dnsDb.groupsOplog.applyOp(operationDoc);
	});
};

module.exports = function(dnsDb, rpc, dnsServerAddress) {
	return new DnsClient(dnsDb, rpc, dnsServerAddress);
};